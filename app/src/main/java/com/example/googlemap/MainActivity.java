package com.example.googlemap;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback{

    private GoogleMap gm;

    private SearchView searchView;

    private SupportMapFragment smf;

    private Button btnVeHinhTron, btnVeDuongBao, btnVeDuongLine, btnClear;

    private LatLng latNow;

    private List<LatLng> lstLatLng = new ArrayList<>();

    private FusedLocationProviderClient fusedLocationClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        searchView = findViewById(R.id.search);

        btnVeHinhTron = findViewById(R.id.btnVeHinhTron);
        btnVeDuongBao = findViewById(R.id.btnVeDuongBao);
        btnVeDuongLine = findViewById(R.id.btnVeDuongLine);
        btnClear = findViewById(R.id.btnClear);

        smf = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.mapFrag);
        smf.getMapAsync(this);

        getLocation();
        function();
    }

    private void getLocation(){
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        fusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        if (location != null) {
                            // Logic to handle location object
                            try {
                                adddMarker(new LatLng(location.getLatitude(), location.getLongitude()), BitmapDescriptorFactory.HUE_GREEN);
                                latNow = new LatLng(location.getLatitude(),location.getLongitude());
                                lstLatLng.add(latNow);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
    }



    private void adddMarker(LatLng latNow, float color) throws IOException {
        gm.addMarker(new MarkerOptions().position(latNow).title(getAddress(latNow.latitude,latNow.longitude)).icon(BitmapDescriptorFactory.defaultMarker(color)));
        CameraPosition cp = new CameraPosition.Builder().target(latNow).zoom(10).build();
        gm.animateCamera(CameraUpdateFactory.newCameraPosition(cp));
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.gm = googleMap;

        gm.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        gm.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                try {
                    adddMarker(latLng, BitmapDescriptorFactory.HUE_ORANGE);
                    lstLatLng.add(latLng);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private String getAddress(double lat, double log) throws IOException {
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());

        addresses = geocoder.getFromLocation(lat,log , 1);

        String address = addresses.get(0).getAddressLine(0);

        return address;
    }

    private void function(){

        btnVeHinhTron.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gm.addCircle(new CircleOptions()
                        .center(latNow)
                        .radius(20000)
                        .fillColor(Color.GREEN)
                        .strokeWidth(5)
                        .strokeColor(Color.RED));

            }
        });


        btnVeDuongBao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PolygonOptions options = new PolygonOptions();

                for(LatLng lat: lstLatLng){
                    options.add(lat);
                }
                options.strokeWidth(4).strokeColor(Color.RED).fillColor(Color.BLUE);
                gm.addPolygon(options);
            }
        });

        btnVeDuongLine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PolylineOptions options = new PolylineOptions();

                for(LatLng lat: lstLatLng){
                    options.add(lat);
                }
                options.width(4).color(Color.BLUE);

                gm.addPolyline(options);
            }
        });

        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gm.clear();
                lstLatLng.clear();
                getLocation();
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                String location = searchView.getQuery().toString();

                List<Address> listAddress = null;

                if (location != null && !location.equals("")) {
                    Geocoder geocoder = new Geocoder(MainActivity.this);

                    try {
                        listAddress = geocoder.getFromLocationName(location, 1);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    if (listAddress != null && listAddress.size() > 0) {
                        Address address = listAddress.get(0);
                        LatLng lat = new LatLng(address.getLatitude(), address.getLongitude());

                        try {
                            adddMarker(lat, BitmapDescriptorFactory.HUE_RED);
                            lstLatLng.add(lat);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
    }


}


