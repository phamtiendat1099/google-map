package com.example.googlemap;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import android.provider.Settings;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.example.googlemap.R;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.io.IOException;
import java.util.List;

public class MainActivity2 extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap gm;
    private SearchView searchView;
    private TextView text;
    private SupportMapFragment smf;
    LatLng latNow;

    FusedLocationProviderClient mFusedLocationClient;
    int PERMISSION_ID = 44;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);


        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        getLastLocation();

//        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
//            @Override
//            public boolean onQueryTextSubmit(String query) {
//                gm.clear();
//                String location = searchView.getQuery().toString();
//
//                List<Address> listAddress = null;
//
//                if (location != null && !location.equals("")) {
//                    Geocoder geocoder = new Geocoder(MainActivity2.this);
//
//                    try {
//                        listAddress = geocoder.getFromLocationName(location, 5);
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//
//                    if (listAddress != null && listAddress.size() > 0) {
//                        Address address = listAddress.get(0);
//
//                        LatLng lat = new LatLng(address.getLatitude(), address.getLongitude());
//                        gm.addMarker(new MarkerOptions().position(lat).title(location));
//
//
//                        float[] results = new float[1];
//                        Location.distanceBetween(latNow.latitude, latNow.longitude,
//                                lat.latitude, lat.longitude,
//                                results);
//
//                        text.setText(String.valueOf(results[0]));
//
//                        CameraPosition cp = new CameraPosition.Builder().target(lat).zoom(10).build();
//                        gm.animateCamera(CameraUpdateFactory.newCameraPosition(cp));
//
//                    }
//                }
//                return false;
//            }
//
//            @Override
//            public boolean onQueryTextChange(String newText) {
//                return false;
//            }
//        });

        createMap();
    }
    private void createMap() {
        smf = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.mapFrag);
        smf.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.gm = googleMap;
    }





    @SuppressLint("MissingPermission")
    private void getLastLocation() {
        if (checkPermissions()) {
            if (isLocationEnabled()) {

                mFusedLocationClient.getLastLocation().addOnCompleteListener(new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        Location location = task.getResult();
                        if (location == null) {
                            requestNewLocationData();
                        } else {
                            latNow = new LatLng(location.getLatitude(),location.getLongitude());
                            if (latNow != null){
                                gm.addMarker(new MarkerOptions().position(latNow).title("CURRENT"));
                                CameraPosition cp = new CameraPosition.Builder().target(latNow).zoom(10).build();
                                gm.animateCamera(CameraUpdateFactory.newCameraPosition(cp));
                            }

                        }
                    }
                });
            } else {
                Toast.makeText(this, "Please turn on" + " your location...", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        } else {
            requestPermissions();
        }
    }

    @SuppressLint("MissingPermission")
    private void requestNewLocationData() {

        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(5);
        mLocationRequest.setFastestInterval(0);
        mLocationRequest.setNumUpdates(1);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
    }

    private LocationCallback mLocationCallback = new LocationCallback() {

        @Override
        public void onLocationResult(LocationResult locationResult) {
            Location location = locationResult.getLastLocation();
            latNow= new LatLng(location.getLatitude(),location.getLongitude());
//            gm.addMarker(new MarkerOptions().position(latNow).title("CURRENT"));
//            CameraPosition cp = new CameraPosition.Builder().target(latNow).zoom(10).build();
//            gm.animateCamera(CameraUpdateFactory.newCameraPosition(cp));

        }
    };

    // method to check for permissions
    private boolean checkPermissions() {
        return ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermissions() {
        ActivityCompat.requestPermissions(this, new String[]{
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_ID);
    }

    private boolean isLocationEnabled() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    @Override
    public void
    onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == PERMISSION_ID) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getLastLocation();
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (checkPermissions()) {
            getLastLocation();
        }
    }
}